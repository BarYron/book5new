<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\book5;

class Book5Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $id = 1; //or later change to 2
     // $id = Auth::id();
      $book5s = User::find($id)->book5s;
      return view('book5s.index',['book5s' => $book5s]); //compact('books')); //compact(todos) equivalent to ['books' => $books]  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('book5s.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book5= new Book5();
        $id=1;
        $book5->title = $request->title;
        $book5->author = $request->author;
        $book5->user_id=$id;
        $book5->save();
        return redirect('book5s');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book5 = Book5::find($id);
        return view('book5s.edit',compact('book5'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book5 = Book5::find($id);
              $book5->update($request->all());
              return redirect('book5s');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book5 = Book5::find($id);
        $book5->delete();
        return redirect('book5s');
    }
}
