<?php

use Illuminate\Database\Seeder;

class Book5TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('book5s')->insert([
            ['author' => 'Yuval noah harari',
            'title' => 'The Histyory of tomaroow',
            'created_at' => date('Y-m-d G:i:s'),
              'user_id' => 1,
             ],
             ['author' => 'Yuval alster',
            'title' => 'the little sea',
            'created_at' => date('Y-m-d G:i:s'),
              'user_id' => 1,
             ]]);
    }
}
